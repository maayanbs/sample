import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private router:Router, private Auth:AuthService) { }

  ngOnInit() {
  }
  toLogin(){
    this.router.navigate(['/login']);

  }
  toLogout(){
    this.Auth.logout().then(value => {
      this.router.navigate(['/login']);
    })
  }
  toItems(){
    this.router.navigate(['/items']);
  }

}
