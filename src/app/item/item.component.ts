import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  price;
  name;
  @Input() data:any;
  constructor() { }

  ngOnInit() {
this.price = this.data.price;
this.name = this.data.$key;
  }

}
