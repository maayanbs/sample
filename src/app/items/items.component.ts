import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
items = [];

  constructor(private db:AngularFireDatabase, private Auth:AuthService) { }
                                          
  ngOnInit() {
    this.Auth.user.subscribe(user => {
      this.db.list('/users/'+'/bob'+'/items').snapshotChanges().subscribe(
        todos =>{                   //this.todos זה זה מלמעלה
                                    //סתם todos יהיה זה מתוך הדטה בייס
        this.items = [];            //נאפס את כל הטודוס
        todos.forEach(                   
        todo => {                          // לכל אחד מהמשתנים במערך נקרא באופן זמני טודו
        let y = todo.payload.toJSON();     //y is a  temp variable,  we fill him (y) with payload us json.
                                             //payload came us string so we need change it to json
        y["$key"] = todo.key;              //אני מוסיף לכל טודו עוד תכונה שיקראו לה דולר קיי ובתוכה יהיה שם המפתח
                                           //key מילה שמורה
        this.items.push(y);                //מכניס את האיבר שיצרתי בתוך טודוס שלנו
    }
      ) 
    }
    
    
    
        )  
    })
  
  }

}